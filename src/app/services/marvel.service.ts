import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MarvelService {
  apiKey  = 'b52022595c2fb1c95ae4abb0dd7cca3c'; 

  constructor(private http: HttpClient) { }


  /**
   * Permite obtener la información de una colección de personajes de la api de Marvel.
   * @returns Colección de personajes.
   */
  getMarvelCharacters(){
    return this.http.get(`https://gateway.marvel.com:443/v1/public/characters?ts=1&apikey=${this.apiKey }&hash=013f863047a30faf31e2d125c86a7505`)
  }

  /**
   * Permite obtener la información de un personaje de la api de Marvel
   * @param id id del personaje
   * @returns Un Personaje en concreto filtrando por id.
   */
  getCharacterDetails(id){
    return this.http.get(`https://gateway.marvel.com:443/v1/public/characters/${id}?ts=1&apikey=${this.apiKey }&hash=013f863047a30faf31e2d125c86a7505`);
  }

}
