import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MarvelService } from '../../services/marvel.service';
import { NavigationExtras, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  dataResults;
  listCharacters = [];

  constructor(private marvelService: MarvelService, private router: Router, private translate: TranslateService) {
    this.translate.setDefaultLang('es');
    this.translate.use('en');
  }

  ngOnInit() {
    //Obtención de los personajes de la API.
    this.marvelService.getMarvelCharacters().subscribe(data => {
      this.dataResults = data;
      this.dataResults.data.results.forEach(element => {
        this.listCharacters.push(element); //Guardo cada personaje en el array listCharacters.
      });
    });
  }

  /**
   * Función que se ejecuta al hacer click en un personaje de la lista. 
   * Navega a la página de detalle del personaje pasándole el id del personaje i el número de posición del personaje en la lista.
   * @param idPassed Id del personaje Clicado
   * @param i Posición en la la lista de items
   */
  clickOnHero(idPassed, i) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        id: idPassed,
        positionHeroClicked: i + 1
      }
    }
    this.router.navigate(["detail"], navigationExtras);
  }

  

}
