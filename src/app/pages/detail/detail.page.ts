import { Component, OnInit } from '@angular/core';
import { MarvelService } from 'src/app/services/marvel.service';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  characterDetailed;
  dataResults;
  idCharacterReceived;
  positionCharacter;

  constructor(private marvelService: MarvelService,private route: ActivatedRoute, private translate: TranslateService) { 
    this.translate.setDefaultLang('es');
    this.translate.use('en');
  }

  ngOnInit() {
    //Obtención de los parámetros pasados de la vista 'List'. 
    this.route.queryParams.subscribe(params => {
      this.idCharacterReceived = params['id'];
      this.positionCharacter = params['positionHeroClicked'];
    });

    //Obtención del personaje de la API.
    this.marvelService.getCharacterDetails(this.idCharacterReceived).subscribe(data => {
      this.dataResults = data;
      this.characterDetailed = this.dataResults.data.results[0]; //Guardo el personaje en una variable que será usada para la vista.
    });
    
  }

}
